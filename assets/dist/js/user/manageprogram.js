$.getJSON("program", function (data) {
    $.each(data.success, function (i, item) {
        // console.log(item);
        var tr = '<tr name="RowProgram"><td>' + item.program_name + '<input type="hidden" name="program_id" value="' + item.program_id + '"></td><td>\n' +
            '<button type="button" class="btn btn-warning" onclick="EditProgram(this)" style="width: 80px">แก้ไข</button>\n' +
            '<button type="button" class="btn btn-danger" onclick="DeleteProgram(this)" style="width: 80px">ลบ</button>\n' +
            '</td></tr>'
        $("#ListAllProgram").append(tr);
    });
});

function EditProgram(this_selected) {
    var program_id = $(this_selected).closest('tr').find('[name=program_id]').val()
    window.location = ('editprogram/'+program_id+'')
}

function DeleteProgram(this_selected) {
    var program_id = $(this_selected).closest('tr').find('[name=program_id]').val();
    alertify.confirm('คำเตือน', 'ยืนยันการลบระบบ'
        , function () {
            $.ajax({
                url: './program/hide',
                async: true,
                dataType: 'json',
                type: 'POST',
                data: {
                    'id': program_id
                },
                success: function (res) {
                    $(this_selected).closest('tr').fadeOut();
                    alertify.success('ลบสำเร็จ')
                },
                error: function (res) {
                    alertify.alert('เกิดข้อผิดพลาด', 'ไม่สามารถลบได้');
                }
            });
        }
        , function () {
            alertify.error('ยกเลิก')
        }
    );
}


function TableSearch(this_val) {
    var value = $(this_val).val().toLowerCase();
    // console.log(value);
    $("#ListAllProgram tr[name=RowProgram]").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}


