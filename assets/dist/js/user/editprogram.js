$("input").prop("disabled", true);
$("select").prop("disabled", true);

$('.select2').select2({
    placeholder: "โปรดเลือกผู้รับผิดชอบ"
})

function AddResponsiblePerson(this_selected) {
    var $lastTr = $(this_selected).closest("#ManageResponsiblePerson").find("[name=GroupResponsiblePerson]:last");
    // console.log($lastTr);
    $lastTr.find('select.select2').select2('destroy'); // Un-instrument original row
    var $clone = $lastTr.clone(); // Clone row
    $(this_selected).closest("#ManageResponsiblePerson").find("[name=GroupResponsiblePerson]:last").after($clone); // Append clone
    $lastTr.find('select.select2').select2({
        placeholder: "โปรดเลือกผู้รับผิดชอบ"
    }); // Re-instrument original row
    $clone.find('select.select2').val('').select2({
        placeholder: "โปรดเลือกผู้รับผิดชอบ"
    }); // Instrument clone
}

function DeleteResponsiblePerson(this_selected) {
    $(this_selected).closest("[name=GroupResponsiblePerson]").fadeOut();
}

function SaveResponsiblePerson() {
    $.ajax({
        url: 'owner/add',
        async: true,
        dataType: 'json',
        type: 'post',
        data: {
            'program_id': $('[name=program_id]').val()
            , 'user_id': $('[name=user_id]').val()
            , 'owner_id': function () {
                var array_no = [];
                $("[name=ResponsiblePerson]").each(function () {
                    array_no.push($(this).val());
                })
                return array_no;
            }()
        },
        success: function (res) {
            console.log(res);
            alertify.alert(res.Success);
            $('[name=program_id]').attr('disabled','disabled')
            $('[name=user_id]').attr('disabled','disabled')
            $("[name=ResponsiblePerson]").attr('disabled','disabled')
        },
        error: function (res) {
            console.log('its not working');
            console.log(res);
        }
    })
}

function AddPermission(this_selected) {
    var GroupSubPermission = $(this_selected).closest("[name=ManagePermission]").find("[name=GroupSubPermission]:last").clone();
    GroupSubPermission.hide();
    $(this_selected).closest("[name=ManagePermission]").find("[name=GroupSubPermission]:last").after(GroupSubPermission);
    GroupSubPermission.fadeIn("slow").find('input[type=text]').val('').removeAttr('funt_id');
}

function DeletePermission(this_selected) {
    $(this_selected).closest("[name=GroupSubPermission]").fadeOut();
    $(this_selected).closest("[name=GroupSubPermission]").remove();
}

function SavePermission(this_selected) {
    var MainPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=MainPermission]');
    var SubPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=SubPermission]');

    $.ajax({
        url: 'role/create',
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            'program_id': $('[name=program_id]').val()
            , 'role': MainPermission.val()
            , 'function': function () {
                var array_no = [];
                SubPermission.each(function () {
                    if($(this).val()){
                        array_no.push($(this).val());
                    }
                })
                return array_no;
            }()
        },
        success: function (res) {
            MainPermission.attr('disabled','disabled')
            SubPermission.attr('disabled','disabled')
            alertify.alert(res.Success);
        },
        error: function (res) {
            console.log('error')
            console.log(res);
        }
    })
}

function EnableProgarmName(this_select) {
    $("[name=SystemName]").removeAttr('disabled');
    $(this_select).remove();
    $("[name=BtnEditProgarmName]").fadeIn("slow");
}

function EditProgarmName() {
    var program_name = $("[name=SystemName]").val();
    var program_id = $("[name=program_id]").val();
    console.log(program_name);
    $.ajax({
        url:'./editprogramdetail',
        async:true,
        dataType:'json',
        type:'post',
        data:{
            id : program_id
            ,data : program_name
        },
        success:function (res) {
            alertify.alert(res.Success);
        },
        error:function (res) {
            console.log('error');
            console.log(res);
        }
    })
}

function EnableResponsiblePerson(this_select) {
    $(this_select).closest("[name=GroupResponsiblePerson]").find("[name=ResponsiblePerson]").removeAttr('disabled');
    $(this_select).hide();
    $(this_select).closest("[name=GroupResponsiblePerson]").find("button[name=BtnEditResponsiblePerson]").fadeIn("slow");
    $(this_select).remove();
}

function EditResponsiblePerson(this_select) {
    var program_id = $("[name=program_id]").val();
    var responsible_id =  $(this_select).closest("[name=GroupResponsiblePerson]").find("[name=ResponsiblePerson]").val();
    console.log(responsible_id,program_id);
    $.ajax({
        url:'',
        dataType:'',
        method:'',
        data:{

        }
    })
}

function EnableMainPermission(this_select) {
    $(this_select).closest("[name=ManagePermission]").find("[name=MainPermission]").removeAttr('disabled');
    $(this_select).hide();
    $(this_select).closest("[name=ManagePermission]").find("button[name=BtnEditMainPermission]").fadeIn("slow");
    $(this_select).remove();
}


function EditMainPermission(this_select) {
    var program_id = $("[name=program_id]").val();
    var mainpermission_val =  $(this_select).closest("[name=ManagePermission]").find("[name=MainPermission]").val();
    var mainpermission_id =  $(this_select).closest("[name=ManagePermission]").find("[name=MainPermission]").data("role_id");
    $.ajax({
        url:'./editmainpermission',
        async:true,
        dataType:'json',
        type:'post',
        data:{
            program_id : program_id
            ,role_id : mainpermission_id
            ,role_name : mainpermission_val
        },
        success:function (res) {
            console.log(res);
            alertify.alert(res.Success);
        },
        error:function (res) {
            console.log('error');
            console.log(res);
        }
    })
}

function AddCardManagePermission() {
    var ManagePermission = $("[new=NewManagePermission]").last().clone();
    ManagePermission.hide();
    $("[name=ManagePermission]").last().after(ManagePermission);
    ManagePermission.fadeIn("slow").find('input[type=text]').val('');
}

function boxWidget(this_selected) {
    $(this_selected).closest('[name=ManagePermission]').boxWidget('toggle')
}

function EnableSubPermission(this_select) {
    $(this_select).closest("[name=GroupSubPermission]").find("[name=SubPermission]").removeAttr('disabled');
    $(this_select).hide();
    $(this_select).closest("[name=GroupSubPermission]").find("button[name=BtnEditSubPermission]").fadeIn("slow");
    $(this_select).remove();
}

function EditSubPermission(this_select) {
    var program_id = $("[name=program_id]").val();
    var subpermission_id =  $(this_select).closest("[name=GroupSubPermission]").find("[funt_id]").attr('funt_id');
    var subpermission_val =  $(this_select).closest("[name=GroupSubPermission]").find("[name=SubPermission]").val();
    var mainpermission_id =  $(this_select).closest("[name=ManagePermission]").find("[name=MainPermission]").data("role_id");
    console.log(subpermission_val,subpermission_id,mainpermission_id,program_id);

   $.ajax({
        url:'./editsubpermission',
        async:true,
        dataType:'json',
        type:'post',
        data:{
            program_id : program_id
            ,role_id : mainpermission_id
            ,function_id : subpermission_id
            ,function_name : subpermission_val
        },
        success:function (res) {
            console.log(res);
            alertify.alert(res.Success);
        },
        error:function (res) {
            console.log('error');
            console.log(res);
        }
    })
}







