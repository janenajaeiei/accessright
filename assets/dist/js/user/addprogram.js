$.getJSON('useritstaff', function (data) {
	$.each(data.Success, function (i, item) {
		// console.log(item);
		var tr = '<option value="' + item.userid + '">' + item.user_nameT + ' ' + item.surnamet + '</option>'
		$("[name=ResponsiblePerson]").append(tr);
	});
});

$('.select2').select2({
	placeholder: "โปรดเลือกผู้รับผิดชอบ"
})

function AddResponsiblePerson(this_selected) {
	var $lastTr = $(this_selected).closest("[name=GroupResponsiblePerson]");
	$lastTr.find('select.select2').select2('destroy'); // Un-instrument original row
	var $clone = $lastTr.clone(); // Clone row
	$(this_selected).closest("[name=GroupResponsiblePerson]").after($clone); // Append clone
	$lastTr.find('select.select2').select2(); // Re-instrument original row
	$clone.find('select.select2').select2(); // Instrument clone
}

function DeleteResponsiblePerson(this_selected) {
	$(this_selected).closest("[name=GroupResponsiblePerson]").fadeOut();
}

function SaveResponsiblePerson() {
	$.ajax({
		url: 'owner/add',
		async: true,
		dataType: 'json',
		type: 'post',
		data: {
			'program_id': $('[name=program_id]').val(),
			'user_id': $('[name=user_id]').val(),
			'owner_id': function () {
				var array_no = [];
				$("[name=ResponsiblePerson]").each(function () {
					array_no.push($(this).val());
				})
				return array_no;
			}()
		},
		success: function (res) {
			console.log(res);
			alertify.alert(res.Success);
			$('[name=program_id]').attr('disabled', 'disabled')
			$('[name=user_id]').attr('disabled', 'disabled')
			$("[name=ResponsiblePerson]").attr('disabled', 'disabled')
		},
		error: function (res) {
			console.log('its not working');
			console.log(res);
		}
	})
}

function AddPermission(this_selected) {
	var GroupSubPermission = $(this_selected).closest("[name=GroupSubPermission]").clone();
	GroupSubPermission.hide();
	$(this_selected).closest("[name=GroupSubPermission]").after(GroupSubPermission);
	GroupSubPermission.fadeIn("slow").find('input[type=text]').val('');
}

function DeletePermission(this_selected) {
	$(this_selected).closest("[name=GroupSubPermission]").fadeOut();
	$(this_selected).closest("[name=GroupSubPermission]").remove();
}

function SavePermission(this_selected) {
	var MainPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=MainPermission]');
	var SubPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=SubPermission]');

	$.ajax({
		url: 'role/create',
		async: true,
		type: 'post',
		dataType: 'json',
		data: {
			'program_id': $('[name=program_id]').val(),
			'role': MainPermission.val(),
			'function': function () {
				var array_no = [];
				SubPermission.each(function () {
					if ($(this).val()) {
						array_no.push($(this).val());
					}
				})
				return array_no;
			}()
		},
		success: function (res) {
			MainPermission.attr('disabled', 'disabled')
			SubPermission.attr('disabled', 'disabled')
			alertify.alert(res.Success);
		},
		error: function (res) {
			console.log('error')
			console.log(res);
		}
	})
}

function SaveSystemDetail() {

	var CountNotValue = 0;
	$('input[type=text]').each(function () {
		if (!$(this).val()) {
			CountNotValue++;
		}
	});
	$('select').each(function () {
		if (!$(this).val()) {
			CountNotValue++;
		}
	});

	if (CountNotValue > 0) {
		alertify.alert('แจ้งเตือน', 'โปรดกรอกข้อมูลให้ครบถ้วนก่อนกดบันทึก');
	} else {
		var SystemName = $("input[name=SystemName]").val();
		console.log(SystemName);

		var responsible_person = [];
		$("[name=ResponsiblePerson]").each(function () {
			responsible_person.push($(this).val());
		})
		console.log(responsible_person);

		var permission = [];
		$("[name=ManagePermission]").each(function () {
			var sub_permission = [];
			var main_permission = [];
			main_permission.push($(this).find('[name=MainPermission]').val());
			$(this).find('[name=SubPermission]').each(function () {
				sub_permission.push($(this).val());
			})
			main_permission.push(sub_permission);
			permission.push(main_permission);
		})
		console.log(permission);

		if (SystemName) {
			$.ajax({
				url: './program/create',
				async: true,
				dataType: 'json',
				type: 'POST',
				data: {
					'program_name': SystemName,
					'owner': responsible_person,
					'data': permission,
					'create_by': $('[name=user_id]').val()
				},
				success: function (res) {
					console.log(res);
					alertify.alert('แจ้งเตือน', res.Success, function () {
						window.location = 'allprogram';
					});
				},
				error: function (res) {
					console.log('error');
					console.log(res);
				}
			});
		} else {
			alertify.alert('โปรดกรอกชื่อระบบ').setHeader('<u>คำเตือน</u>');;
		}
	}
}

function AddCardManagePermission() {
	var ManagePermission = $("[name=ManagePermission]").last().clone();
	ManagePermission.hide();
	$("[name=ManagePermission]").last().after(ManagePermission);
	ManagePermission.fadeIn("slow").find('input[type=text]').val('').removeAttr('disabled');
}

function boxWidget(this_selected) {
	$(this_selected).closest('[name=ManagePermission]').boxWidget('toggle');
}



