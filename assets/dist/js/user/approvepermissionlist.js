$('.content-wrapper').loading({
    theme: 'dark'
});

$.getJSON("getapprovelist", function (data) {
    console.log(data);
    if(data.Success){
        $.each(data.Success, function (i, item) {
            var tr = '<tr name="ApproveList">' +
                '<td><a href="./approvepermissionform/'+item.program_id+'/'+item.ID+'">'+item.ID+'</a></td>\n' +
                '<td>'+item.request_date+'</td>\n' +
                '<td></td>\n' +
                '</tr>'
            $("tbody").append(tr);
        });
    }else{
        var tr = '<tr name="ApproveList">' +
            '<td colspan="3" style="text-align: center;font-style: italic">ไม่มีข้อมูล</td>\n' +
            '</tr>'
        $("tbody").append(tr);
    }
    $('.content-wrapper').loading('stop');
});


