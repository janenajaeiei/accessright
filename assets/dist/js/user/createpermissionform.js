$('.content-wrapper').loading({
	theme: 'dark'
});

$.getJSON("program", function (data) {
	$.each(data.success, function (i, item) {
		var tr = '<option value="' + item.program_id + '">' + item.program_name + '</option>'
		$("[name=ProgramName]").append(tr);
	});
	$.getJSON("getapprovername", {
		cmp: $('[name=user_cmp]').val()
	}).done(function (data) {
		$.each(data.Success, function (i, item) {
			var tr = '<option value="' + item.UserID + '">' + item.User_NameT + ' ' + item.SurNameT + '</option>'
			$("[name=ApproverName]").append(tr);

		});
		$('.content-wrapper').loading('stop');
	});
});

$('.select2').select2({
	placeholder: "โปรดเลือกชื่อระบบ"
});

function SelectedProgram(this_selected) {
	$.ajax({
		url: 'getprogramdetail',
		async: true,
		dataType: 'json',
		type: 'post',
		data: {
			'program_id': $(this_selected).val()
		},
		success: function (res) {
			$('#role').html('');
			// console.log(res.Success.role);
			var role_datail = res.Success.role;
			$(role_datail).each(function (i, item) {
				// var td_2_div_input = '';
				var td_2_div_input = [];
				var tbody = $('#role');
				var row = $('<tr></tr>').hide();
				var td_1 = $('<td data-role-id="' + item.role_id + '"></td>');
				var td_2 = $('<td></td>');
				var td_2_div = $('<div ></div>');
				td_1.html(item.role_name);
				$(item.func[0]).each(function (i, item) {
					// td_2_div_input += ('<input type="checkbox">'+item.function_name+'');
					td_2_div_input.push('<input type="checkbox" class="minimal" value="' + item.function_id + '">&nbsp;' + item.function_name + '');
				})
				td_2_div_input = td_2_div_input.join('&emsp;&emsp;')
				// console.log(td_2_div_input);
				td_2_div.append(td_2_div_input);
				td_2.append(td_2_div);
				row.append(td_1);
				row.append(td_2);
				tbody.append(row);
				row.fadeIn('slow');
				console.log(row[0]);
				// console.log(td_2_div_input[0]);

				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
					checkboxClass: 'icheckbox_minimal-blue',
					radioClass: 'iradio_minimal-blue'
				})
			})
		},
		error: function (res) {
			console.log('error');
			console.log(res);
		}
	})
}

function SendReqestReport() {

	var CountNotValue = 0;
	var CountNotChk = 0;

	$('input').each(function () {
		if (!this) {
			CountNotValue++;
		}
	})
	$('select').each(function () {
		if (!$(this).val()) {
			CountNotValue++;
		}
	});
	$('[type=checkbox]').each(function () {
		if ($(this).is(':checked')) {
			CountNotChk++;
		}
    });
    // console.log(CountNotChk);

	if (CountNotValue > 0) {
        alertify.alert('แจ้งเตือน','โปรดกรอกข้อมูลให้ครบถ้วน');
	} else if(CountNotChk == 0){
		alertify.alert('แจ้งเตือน','โปรดเลือกสิทธิอย่างน้อย 1 สิทธิ');
	}else{
        var user_id = $('[name=user_id]').val();
		var user_name = $('[name=user_name]').val();;
		var user_surname = $('[name=user_surname]').val();;
		var program_id = $("[name=ProgramName] :selected").val();
		var approver_id = $("[name=ApproverName] :selected").val();
		var change_motive_id = $("[name=MotiveChangePermission] :selected").val();
		var change_type_id = $("[name=TypeChangePermission] :selected").val();
		var row_sequence = 0;
		var data_role_funt = {};

		$("#role").find('tr').each(function () {
			row_sequence++;
			var funt_id = [];
			$(this).find('input[type=checkbox]').each(function () {
				if ($(this).is(':checked')) {
					funt_id.push($(this).val());
				}
			})
			if (funt_id.length > 0) {
				var role_id = $(this).find('td:first').data("role-id");
				var role_funt_id = [role_id];
				role_funt_id.push(funt_id);
			}
			if (role_funt_id) {
				data_role_funt[row_sequence] = role_funt_id;
			}
		});

		console.log(program_id);
		console.log(change_motive_id);
		console.log(change_type_id);
		console.log(data_role_funt);
		console.log(approver_id);

		$.ajax({
			url: 'sendform',
			async: true,
			dataType: 'json',
			type: 'post',
			data: {
				'program_id': program_id,
				'user_id': user_id,
				'user_name': user_name,
				'user_surname': user_surname,
				'cuase': change_motive_id,
				'type': change_type_id,
				'approver': approver_id,
				'function_data': data_role_funt
			},
			success: function (res) {
				alertify.alert('แจ้งเตือน',res.Success,function(){
                    window.location = "./requestpermissionlist";
                });
			},
			error: function (res) {
				console.log('error');
				console.log(res);
			}
		})
    }
}
