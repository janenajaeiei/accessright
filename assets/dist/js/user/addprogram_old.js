$.getJSON('useritstaff', function (data) {
    $.each(data.Success, function (i, item) {
        // console.log(item);
        var tr = '<option value="' + item.userid + '">' + item.user_nameT + ' ' + item.surnamet + '</option>'
        $("[name=ResponsiblePerson]").append(tr);
    });
});

function AddResponsiblePerson(this_selected) {
    var $lastTr = $(this_selected).closest("[name=GroupResponsiblePerson]");
    $lastTr.find('select.select2').select2('destroy'); // Un-instrument original row
    var $clone = $lastTr.clone(); // Clone row
    $(this_selected).closest("[name=GroupResponsiblePerson]").after($clone); // Append clone
    $lastTr.find('select.select2').select2(); // Re-instrument original row
    $clone.find('select.select2').select2(); // Instrument clone
}

function DeleteResponsiblePerson(this_selected) {
    $(this_selected).closest("[name=GroupResponsiblePerson]").fadeOut();
}

function SaveResponsiblePerson() {
    $.ajax({
        url: 'owner/add',
        async: true,
        dataType: 'json',
        type: 'post',
        data: {
            'program_id': $('[name=program_id]').val()
            , 'user_id': $('[name=user_id]').val()
            , 'owner_id': function () {
                var array_no = [];
                $("[name=ResponsiblePerson]").each(function () {
                    array_no.push($(this).val());
                })
                return array_no;
            }()
        },
        success: function (res) {
            console.log(res);
            alertify.alert(res.Success);
            $('[name=program_id]').attr('disabled','disabled')
            $('[name=user_id]').attr('disabled','disabled')
            $("[name=ResponsiblePerson]").attr('disabled','disabled')
        },
        error: function (res) {
            console.log('its not working');
            console.log(res);
        }
    })
}

function AddPermission(this_selected) {
    var GroupSubPermission = $(this_selected).closest("[name=GroupSubPermission]").clone();
    GroupSubPermission.hide();
    $(this_selected).closest("[name=GroupSubPermission]").after(GroupSubPermission);
    GroupSubPermission.fadeIn("slow").find('input[type=text]').val('');
}

function DeletePermission(this_selected) {
    $(this_selected).closest("[name=GroupSubPermission]").fadeOut();
    $(this_selected).closest("[name=GroupSubPermission]").remove();
}

function SavePermission(this_selected) {
    var MainPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=MainPermission]');
    var SubPermission = $(this_selected).closest('[name=ManagePermission]').find('[name=SubPermission]');

    $.ajax({
        url: 'role/create',
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            'program_id': $('[name=program_id]').val()
            , 'role': MainPermission.val()
            , 'function': function () {
                var array_no = [];
                SubPermission.each(function () {
                    if($(this).val()){
                        array_no.push($(this).val());
                    }
                })
                return array_no;
            }()
        },
        success: function (res) {
            MainPermission.attr('disabled','disabled')
            SubPermission.attr('disabled','disabled')
            alertify.alert(res.Success);
        },
        error: function (res) {
            console.log('error')
            console.log(res);
        }
    })
}

function SaveSystemName() {
    var SystemName = $("input[name=SystemName]").val();
    if (SystemName) {
        $.ajax({
            url: './program/create',
            async: true,
            dataType: 'json',
            type: 'POST',
            data: {
                'program_name': SystemName
            },
            success: function (res) {
                $("#ManageResponsiblePerson").fadeIn("slow");
                $("[name=ManagePermission]").fadeIn("slow");
                $("[name=ButtonAddCardManagePermission]").fadeIn("slow");
                $("[name=program_id]").val(res.program_id);
                $("input[name=SystemName]").attr('disabled','disabled')
                alertify.alert('แจ้งเตือน',res.Success);
            },
            error: function (res) {
                console.log('error');
                console.log(res);
            }
        });
    } else {
        alertify.alert('โปรดกรอกชื่อระบบ').setHeader('<u>คำเตือน</u>');
        ;
    }
}

function AddCardManagePermission() {
    var ManagePermission = $("[name=ManagePermission]").last().clone();
    ManagePermission.hide();
    $("[name=ManagePermission]").last().after(ManagePermission);
    ManagePermission.fadeIn("slow").find('input[type=text]').val('').removeAttr('disabled');
}

function boxWidget(this_selected) {
    $(this_selected).closest('[name=ManagePermission]').boxWidget('toggle');
}





