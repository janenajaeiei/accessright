<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader
{
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $vars['__html'] = array(
            'scripts' => $this->view('ext/script', $vars, TRUE),
            'styles' => $this->view('ext/stylesheet', $vars, TRUE),
            'menu' => $this->view('ext/menu', $vars, TRUE),
            'header' => $this->view('ext/header', $vars, TRUE),
            'content' => $this->view('include/' . $template_name, $vars, TRUE)
        );

        return $this->view('template', $vars, $return);
    }
}
