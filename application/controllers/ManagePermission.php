<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManagePermission extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $sso = new SSO();
        $this->session = $sso->getAuthentication();
        $this->load->model('user_model');
    }

    public function createpermissionform()
    {
        $data['profile_pic']= $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $this->load->template("createpermissionform",$data);
    }

    public function RequsetPermissionList()
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $this->load->template("requestpermissionlist", $data);
    }

    public function ApprovePermissionList()
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $this->load->template("Approvepermissionlist", $data);
    }

    public function ApprovePermissionForm($pid,$rqid)
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $data['program'] = $this->program_model->get_program_detail($pid);
        if ($data['program'] != false) {
            $data['role'] = $this->program_model->get_role_detail($pid);
        }
        $data['requestdetail'] = $this->request_access_model->get_request_line($pid,$rqid);
        $data['head'] = $this->request_access_model->get_request_detail($rqid);
        $this->load->template("Approvepermissionform", $data);
    }


}
