<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
    {
        parent::__construct();
		$sso   = new SSO();
         $this->session = $sso->getAuthentication();
        $this->load->model('user_model');
	}
    public function getAllIT()
    {
        $data = $this->user_model->get_all_it_person();
        echo json_encode(array("Success"=>$data));
    }

    public function get_approver()
    {
        $data=$this->user_model->get_approver($this->input->get('cmp'));
        echo json_encode(array("Success"=>$data));
    }
}
