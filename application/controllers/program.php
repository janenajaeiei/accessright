<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller
{
    public function createprogram()
    {
        $program_name =$this->input->post('program_name');
        $create_by = $this->input->post('create_by');
       // print_r($program_name);
        $pid = $this->program_model->createprogram(array("program_name"=>$program_name));
        foreach ($this->input->post('owner') as  $value) {
            $data_insert = array("program_id"=>$pid,'owner_id'=>$value,'create_by'=>$create_by);
            $this->program_model->add_program_owner($data_insert);
        
        }
        if($this->input->post('data')){
            $data_role = $this->input->post('data');
            foreach ($data_role  as $value) {
                
                $role_id = $this->program_model->add_new_role($pid, $value[0]);
                if($value[1]){
                    $this->program_model->add_program_function($pid, $role_id[0]['role_id'], $value[1]);
                }
            }
        }
        echo json_encode(array("Success" => "บันทึกข้อมูลสำเร็จแล้ว"));
    }

    public function getallprogram()
    {
        $data = $this->program_model->getallprogramshow();
        if ($data != 0)
            echo json_encode(array("success" => $data));
        else  echo json_encode(array("error" => "ยังไม่มีข้อมูลของโปรแกรมในขณะนี้"));
    }

    public function hideprogram()
    {
        $this->program_model->hideprogram($this->input->post('id'));
        echo json_encode(array("Success" => "ลบโปรแกรมออกจากรายการแล้ว"));
    }

    public function editprogramdetail()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        // print_r($this->input->post());
        $this->program_model->editprogramdetail($id, $data);
        echo json_encode(array("Success" => "บันทึกข้อมูลสำเร็จแล้ว"));
        # code...
    }

    public function unhideprogram()

    {
        $this->input->post('id');
        $this->program_model->unhideprogram($this->input->post('id'));

    }

    public function addrole()
    {
        $pid = $this->input->post('program_id');
        $data_role = $this->input->post('role');
        $data_funtion = $this->input->post('function');
        $role_id = $this->program_model->add_new_role($pid, $data_role);
        if($data_funtion){
            $this->program_model->add_program_function($pid, $role_id[0]['role_id'], $data_funtion);
        }
//        echo $role_id[0]['role_id'];
        echo json_encode(array("Success" => "บันทึกข้อมูลสำเร็จแล้ว"));
    }

    public function hiderole()
    {
        $this->program_model->hide_role($this->input->post('role_id'), $this->input->post('program_id'));
        echo json_encode(array("Success" => "ลบสิทธิ์การใช้งานออกจากรายการแล้ว"));
    }

    public function unhiderole()
    {
        $this->program_model->unhide_role($this->input->post('role_id'), $this->input->post('program_id'));
        echo json_encode(array("Success" => "บันทึกรายการเรียบร้อยแล้ว"));
    }
    public function editrole()
    {
        $this->program_model->edit_role_name($this->input->post('program_id'), $this->input->post('role_id'), $this->input->post('role_name')); # code...
        echo json_encode(array("Success" => "บันทึกรายการเรียบร้อยแล้ว"));
    }
    public function editfunction()
    {
        if($this->input->post('role_id')){
        $this->program_model->edit_function( $this->input->post('program_id'),$this->input->post('role_id'), $this->input->post('function_id'), $this->input->post('function_name')); # code...
        echo json_encode(array("Success" => "บันทึกรายการเรียบร้อยแล้ว"));
        }
        else{
            $this->program_model->add_program_function($this->input->post('program_id'),$this->input->post('role_id'),$this->input->post('function_name'));
            echo json_encode(array("Success" => "เพิ่มรายการเรียบร้อยแล้ว"));
        }
    }
    public function get_program_role()
    {
        $data = $this->program_model->get_role($this->input->post('program_id')); # code...
        if ($data != 0) echo json_encode(array("Success" => $data));
        else echo json_encode(array("Error" => "ไม่พบข้อมูลสิทธิ์การใช้งานของโปรแกรมนี้"));
    }

    public function add_program_owner()
    {
        $program_id = $this->input->post('program_id');
        $owner_id = $this->input->post('owner_id');
        $user_id = $this->input->post('user_id');
        $this->program_model->add_program_owner($program_id, $owner_id, $user_id);
        echo json_encode(array("Success" => "บันทึกรายการเรียบร้อยแล้ว"));
    }

    public function get_programDetail()
    {
        $data['program'] = $this->program_model->get_program_detail($this->input->post('program_id'));
//        $data['program_owner'] = $this->program_model->get_program_owner($this->input->post('program_id'));
        if ($data['program'] != false) {
            $data['role'] = $this->program_model->get_role_detail($this->input->post('program_id'));
            echo json_encode(array("Success" => $data));
        } else json_encode(array("Error" => "ไม่พบโปรแกรมนี้"));
    }
}

/* End of file Program.php */

?>