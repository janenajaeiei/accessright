<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class RequestAccess extends CI_Controller {
        function __construct()
    {
        parent::__construct();
        $sso = new SSO();
        $this->session = $sso->getAuthentication();
    }
    
        public function create_request()
        {
            $program_id =$this->input->post('program_id');
            $user_id =$this->input->post('user_id');
            $user_name =$this->input->post('user_name');
            $user_surname =$this->input->post('user_surname');
            $cuase =$this->input->post('cuase');
            $type =$this->input->post('type');
            $approver =$this->input->post('approver');
            $function_data =$this->input->post('function_data');
          
            $data_insert = array("program_id"=>$program_id,"user_id"=>$user_id,"user_name"=>$user_name,"user_surname"=>$user_surname,"cuase"=>$cuase,"type"=>$type,"approved_by"=>$approver,"approve_status"=>0,"it_approve_status"=>0);
            $this->request_access_model->newrequest($data_insert,$function_data);

            echo json_encode(array("Success"=>"บันทึกข้อมูลเรียบร้อยแล้ว"));
        }

        public function get_request_by_owner()
        {
           $data=$this->request_access_model->get_request_by_owner($this->input->post('uid'));
           echo json_encode(array("Success"=>$data));
        }
        public function get_request_by_approver()
        {
           $data=$this->request_access_model->get_request_by_approver($this->session['personDetail']['UserID']);
           echo json_encode(array("Success"=>$data));
        }
        public function get_request_line()
        {
            $pid = $this->input->post('pid');
            $rqid = $this->input->post('rqid');
            $data = $this->request_access_model->get_request_line($pid,$rqid);
            $data['head'] = $this->request_access_model->get_request_detail($rqid);
            echo json_encode(array("Success"=>$data));
        }

        public function get_request_by_user()
        {
            $data = $this->request_access_model->get_all_request_by_user($this->session['personDetail']['UserID']);
            echo json_encode(array("Success"=>$data));
        }
       public function approve_by_senior()
       {
        
        $this->request_access_model->approver_by_senior($this->input->post('rqid'));
        echo json_encode(array("Success"=>"อนุมัติคำขอเรียบร้อยแล้ว"));
       }

       public function approve_by_it()
       {
        
        $this->request_access_model->approver_by_it($this->input->post('rqid'));
        echo json_encode(array("Success"=>"อนุมัติคำขอเรียบร้อยแล้ว"));
       }
    }
    
    /* End of file Controllername.php */
    