<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageProgram extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $sso = new SSO();
        $this->session = $sso->getAuthentication();
        $this->load->model('user_model');
    }

    public function AddProgram()
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $this->load->template("addprogram", $data);
    }

    public function MainManageProgram()
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $this->load->template("manageprogram", $data);
    }

    public function EditProgram($id)
    {
        $data['profile_pic'] = $this->user_model->getpic($this->session);
        $data['profile_detail'] = $this->session;
        $data['profile'] = $this->user_model->getprofiledetail($this->session);
        $data['program'] = $this->program_model->get_program_detail($id);
        $data['program_owner'] = $this->program_model->select_program_owner($id);
        $data['ItStaff'] = $this->user_model->get_all_it_person();
        if ($data['program'] != false) {
            $data['role'] = $this->program_model->get_role_detail($id);
        }
        $this->load->template("editprogram", $data);
    }
}
