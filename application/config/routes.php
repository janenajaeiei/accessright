
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['program/create']['post'] = 'program/createprogram';
$route['program']['get'] = 'program/getallprogram';
$route['program/hide']['post'] = 'program/hideprogram';
$route['program/edit']['post'] = 'program/editprogramdetail';
$route['program/unhide']['post'] = 'program/unhideprogram';
$route['role/create']['post'] = 'program/addrole';
$route['role/hide']['post'] = 'program/hiderole';
$route['role/unhide']['post'] = 'program/unhiderole';
$route['role/edit']['post'] = 'program/editrole';
$route['role/getrole']['post'] = 'program/get_program_role';
$route['function/edit']['post']='program/editfunction';
$route['owner/add']['post'] = 'program/add_program_owner';
$route['getprogramdetail'] = 'program/get_programDetail';
$route['useritstaff'] = 'user/getAllIT';


//----------------------- Add Program ---------------------//
$route['addprogram'] = 'ManageProgram/AddProgram';
$route['allprogram'] = 'ManageProgram/MainManageProgram';

//----------------------- Edit Program ---------------------//
$route['editprogram/(:num)'] = 'ManageProgram/EditProgram/$1';
$route['editprogram/getprogramdetaill']['post'] = 'program/get_programDetail';
$route['editprogram/editprogramdetail'] = 'program/editprogramdetail';
$route['editprogram/editmainpermission'] = 'program/editrole';
$route['editprogram/editsubpermission'] = 'program/editfunction';

//----------------------- Link Page ---------------------//
$route['default_controller'] = 'ManagePermission/RequsetPermissionList';
$route['requestpermissionlist'] = 'ManagePermission/RequsetPermissionList';
$route['approvepermissionlist'] = 'ManagePermission/ApprovePermissionList';
$route['createpermissionform'] = 'ManagePermission/createpermissionform';

//----------------------- Permission ---------------------//
$route['sendform'] = 'requestaccess/create_request';
$route['getapprovername']['get'] = 'user/get_approver';
$route['getrequestlist']['get'] = 'requestaccess/get_request_by_user';
$route['getapprovelist']['get'] = 'requestaccess/get_request_by_approver';
$route['approvepermissionform/(:num)/(:num)'] = 'ManagePermission/ApprovePermissionForm/$1/$2';






