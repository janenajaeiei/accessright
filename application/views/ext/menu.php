<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://corporate.precise.group/Intranet/images/emp/<?=$profile_pic?>" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $profile_detail['personDetail']['PersonFnamethai']."    ".$profile_detail['personDetail']['PersonLnamethai']  ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo base_url('requestpermissionlist'); ?>" >
                    <i class="fa fa-file-o"></i>
                    <span>ขอสิทธิ์การใช้งานระบบ</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('approvepermissionlist'); ?>" >
                    <i class="fa fa-check-square-o"></i>
                    <span>อนุมัติสิทธิ์การใช้งานระบบ</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('addprogram'); ?>" >
                    <i class="fa fa-clone"></i>
                    <span>เพิ่มระบบ</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('allprogram'); ?>" >
                    <i class="fa fa-wrench"></i>
                    <span>ลบ/แก้ไขระบบ</span>
                </a>
            </li>
        </ul>
    </section>
</aside>