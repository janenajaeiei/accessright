<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">จัดการระบบ</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" onkeyup="TableSearch(this)" class="form-control pull-right"
                               placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" disabled style="cursor: default" class="btn btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="ListAllProgram">
                    <tr>
                        <th style="width: 50%">ชื่อระบบ</th>
                        <th style="width: 50%">จัดการ</th>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/manageprogram.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/accessright.js') ?>"></script>

