<div class="content-wrapper">
    <section class="content-header">
        <h1>
            ฟอร์มขอสิทธิ์การเข้าใช้งานระบบ
        </h1>
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">โปรดเลือกระบบ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>ชื่อระบบ</label>
                            <select class="form-control select2" name="ProgramName" onchange="SelectedProgram(this);" style="width: 100%;">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">ฟอร์มข้อมูล</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สาเหตุการเปลี่ยนแปลงสิทธิ์</label>
                            <select class="form-control" style="width: 100%;" name="MotiveChangePermission">
                                <option selected="selected" disabled selected value> -- โปรดเลือกสาเหตุ -- </option>
                                <option value="1">ย้ายบริษัท</option>
                                <option value="2">ย้ายแผนก</option>
                                <option value="3">ลาออก</option>
                                <option value="4">อื่นๆ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ประเภทการเปลี่ยนแปลงสิทธิ์</label>
                            <select class="form-control" style="width: 100%;" name="TypeChangePermission">
                                <option selected="selected" disabled selected value=""> -- โปรดเลือกประเภท -- </option>
                                <option value="1">เพิ่มสิทธิ์</option>
                                <option value="2">ลดสิทธิ์</option>
                                <option value="3">ยกเลิกการใช้งานระบบ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:20%;">ชื่อสิทธิ์</th>
                                    <th style="width:80%;">รายละเอียด</th>
                                </tr>
                                </thead>
                                <tbody id="role">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">ผู้ร้องขอ</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th style="width: 25%">ชื่อผู้ใช้</th>
                        <th style="width: 25%">วันที่</th>
                        <th style="width: 25%">สถานะ</th>
                        <th style="width: 25%">ตำแหน่ง</th>
                    </tr>
                    <tr>
                        <td><?php echo $profile_detail['personDetail']['PersonFnamethai']."    ".$profile_detail['personDetail']['PersonLnamethai']; ?></td>
                        <td><?php echo date("d/m/Y"); ?></td>
                        <td><span class="label label-warning">Pending</span></td>
                        <td>ผู้ขอเปลี่ยนแปลงสิทธิ์</td>
                    </tr>
                    <tr>
                        <td><?php echo $profile_detail['personDetail']['PersonFnamethai']."    ".$profile_detail['personDetail']['PersonLnamethai']  ?></td>
                        <td><?php echo date("d/m/Y"); ?></td>
                        <td><span class="label label-warning">Pending</span></td>
                        <td>ผู้ใช้งาน</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">ผู้อนุมัติ</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th style="width: 25%">ชื่อผู้ใช้</th>
                        <th style="width: 25%">วันที่</th>
                        <th style="width: 25%">สถานะ</th>
                        <th style="width: 25%">ตำแหน่ง</th>
                    </tr>
                    <tr>
                        <td>
                            <select name="ApproverName" class="form-control" style="width: 80%">
                                <option disabled selected value=""> -- โปรดเลือกผู้อนุมัติ -- </option>
                            </select>
                        </td>
                        <td>-</td>
                        <td><span class="label label-success"></span>-</td>
                        <td>DM/Asst.BM/GM/BM/MD</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td><span class="label label-warning"></span>-</td>
                        <td>Administrator</td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <div style="text-align: center">
        <button type="button" class="btn btn-lg" style="width: 40%;">ย้อนกลับ</button>
        &emsp;
        <button type="button" class="btn btn-success btn-lg" style="width: 40%;" onclick="SendReqestReport()">บันทึก</button>
    </div>
    <br><br>
    <input type="hidden" name="user_id" value="<?php echo $profile_detail['personDetail']['UserID']?>">
    <input type="hidden" name="user_name" value="<?php echo $profile_detail['personDetail']['PersonFnamethai']?>">
    <input type="hidden" name="user_surname" value="<?php echo $profile_detail['personDetail']['PersonLnamethai']; ?>">
    <input type="hidden" name="user_cmp" value="<?php echo $profile_detail['personDetail']['CompanyCode']; ?>">
</div>

<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/createpermissionform.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/accessright.js') ?>"></script>


