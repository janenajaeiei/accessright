
<div class="content-wrapper">
    <section class="content-header">
        <h1> 
            แก้ไขระบบ
        </h1>

    </section>
    <section class="content">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการชื่อระบบ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" onsubmit="EditProgarmName();return false">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อระบบ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="SystemName"
                                               value="<?=$program[0]['program_name']?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set" style="padding-right: 36px">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableProgarmName(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditProgarmName"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditProgarmName()">บันทึก
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-warning" id="ManageResponsiblePerson">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการผู้รับผิดชอบ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <?php if($program_owner){ ?>
                                <?php foreach($program_owner as $key => $value){ ?>
                                <div class="form-group" name="GroupResponsiblePerson">
                                    <label class="col-sm-2 control-label">ชื่อผู้รับผิดชอบ</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2" name="ResponsiblePerson"
                                                style="width: 100%;">
                                            <option></option>
                                            <?php foreach($ItStaff as $key => $value_allstaff){ ?>
                                            <option value="<?=$value_allstaff['userid']?>"
                                            <?=($value['UserID'] == $value_allstaff['userid'])?'selected':null ?> >
                                            <?=$value_allstaff["user_nameT"].' '.$value_allstaff["surnamet"]?> </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableResponsiblePerson(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditResponsiblePerson"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditResponsiblePerson(this)">
                                                    บันทึก
                                                </button>
                                            </div>
                                            &nbsp;
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeleteResponsiblePerson(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                    } 
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="button" class="btn btn-success" onclick="AddResponsiblePerson(this)">เพิ่ม</button>
            </div>
        </div>

        <?php
        if(is_array($role)){
        foreach($role as $key => $value){ ?>
        <div class="box box-warning" name="ManagePermission" togglebox="MyBox">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการสิทธื์<input type="hidden" name="PermissionID"
                                                         value="<?=$value['role_id']?>">
                </h3>
                <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i
                            class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" onclick="RemovePermission(this)"><i
                            class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสิทธิ์</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="MainPermission" data-role_id="<?=$value['role_id']?>"
                                               value="<?=$value['role_name']?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set" style="padding-right: 36px">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableMainPermission(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditMainPermission"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditMainPermission(this)">บันทึก
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                 if(is_array($value["func"][0])){
                                    foreach($value["func"][0] as $key => $value_func){
                                        if(isset($value_func["role_id"])){
                                            if($value_func["role_id"] == $value["role_id"]){
                                ?>
                                <div class="form-group" name="GroupSubPermission">
                                    <label class="col-sm-3 control-label">ชื่อประเภท</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="SubPermission" funt_id="<?=$value_func['function_id']?>"
                                               value="<?=$value_func['function_name']?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableSubPermission(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditSubPermission"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditSubPermission(this)">
                                                    บันทึก
                                                </button>
                                            </div>
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeletePermission(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                            }
                                        }
                                    }
                                }else{
                                ?>
                                <div class="form-group" name="GroupSubPermission">
                                    <label class="col-sm-3 control-label">ชื่อประเภท</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="SubPermission"
                                               value="">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="">แก้ไข
                                                </button>
                                            </div>
                                            &nbsp;
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeletePermission(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="button" class="btn btn-success" onclick="AddPermission(this)">เพิ่ม</button>
            </div>
        </div>
        <?php
        }
        }else{
        ?>
        <div class="box box-warning" name="ManagePermission" togglebox="MyBox">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการสิทธื์</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสิทธิ์</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="MainPermission">
                                    </div>
                                </div>
                                <div class="form-group" name="GroupSubPermission">
                                    <label class="col-sm-3 control-label">ชื่อประเภท</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="SubPermission">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="">แก้ไข
                                                </button>
                                            </div>
                                            &nbsp;
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeletePermission(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="button" class="btn btn-success" onclick="AddPermission(this)">เพิ่ม</button>
            </div>
        </div>

        <?php
        }
        ?>

        <div class="box box-info" name="ManagePermission" new="NewManagePermission"  style="display: none">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการสิทธื์</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="toggle" onclick="boxWidget(this)">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสิทธิ์</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="MainPermission">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set" style="padding-right: 36px">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableMainPermission(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditMainPermission"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditMainPermission(this)">บันทึก
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" name="GroupSubPermission">
                                    <label class="col-sm-3 control-label">ชื่อประเภท</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="SubPermission">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-warning btn-sm"
                                                        onclick="EnableSubPermission(this)">แก้ไข
                                                </button>
                                                <button type="button" name="BtnEditSubPermission"
                                                        class="btn btn-block btn-info btn-sm"
                                                        style="display: none" onclick="EditSubPermission(this)">
                                                    บันทึก
                                                </button>
                                            </div>
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeletePermission(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-footer" style="text-align: center">
                <button type="button" class="btn btn-success" onclick="AddPermission(this)">เพิ่ม</button>
            </div>
        </div>

    </section>
    <input type="hidden" value="<?=$program[0]['program_id']?>" name="program_id">
    <input type="hidden" value="<?=$profile_detail['personDetail']['UserID']?>" name="user_id">
    <div style="text-align: center">
        <button type="button" class="btn btn-info btn-lg" onclick="AddCardManagePermission(this)"
                name="ButtonAddCardManagePermission"
                style="width: 30%;">เพิ่มสิทธิ์
        </button>
    </div>
    <br><br>
</div>

<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/editprogram.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/accessright.js') ?>"></script>

