<div class="content-wrapper">
	<section class="content-header">
		<h1>
			รายการขออนุมัติของคุณ
		</h1>
		<button type="button" class="btn btn-info" style="margin-top: 10px;width: 200px" onclick="location.href='createpermissionform';">สร้างฟอร์ม</button>
	</section>
	<section class="content">
		<div class="box box-info">
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 25%">ใบคำร้องเลขที่</th>
							<th style="width: 25%">วันที่</th>
							<th style="width: 25%">สถานะ</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/requestpermissionlist.js') ?>"></script>
