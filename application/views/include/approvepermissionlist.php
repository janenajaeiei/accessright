<div class="content-wrapper">
    <section class="content-header">
        <h1>
            รายการรออนุมัติของคุณ
        </h1>
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th style="width: 25%">ใบคำร้องเลขที่</th>
                        <th style="width: 25%">วันที่</th>
                        <th style="width: 25%">สถานะ</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>


<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/approvepermissionlist.js') ?>"></script>

