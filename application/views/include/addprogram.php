<div class="content-wrapper">
    <section class="content-header">
        <h1>
            เพิ่มระบบ
        </h1>
    </section>
    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการชื่อระบบ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" onsubmit="SaveSystemName();return false;">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อระบบ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="SystemName">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-info" id="ManageResponsiblePerson" >
            <div class="box-header with-border">
                <h3 class="box-title">จัดการผู้รับผิดชอบ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group" name="GroupResponsiblePerson">
                                    <label class="col-sm-2 control-label">ชื่อผู้รับผิดชอบ</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2" name="ResponsiblePerson" style="width: 100%;">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-success btn-sm"
                                                        onclick="AddResponsiblePerson(this)">เพิ่ม
                                                </button>
                                            </div>
                                            &nbsp;
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeleteResponsiblePerson(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-info" name="ManagePermission" togglebox="MyBox">
            <div class="box-header with-border">
                <h3 class="box-title">จัดการสิทธื์</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" onclick="boxWidget(this)"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" ><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสิทธิ์</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="MainPermission">
                                    </div>
                                </div>
                                <div class="form-group" name="GroupSubPermission">
                                    <label class="col-sm-3 control-label">ชื่อประเภท</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="SubPermission">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row btn-set">
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-success btn-sm"
                                                        onclick="AddPermission(this)">เพิ่ม
                                                </button>
                                            </div>
                                            &nbsp;
                                            <div class="inner">
                                                <button type="button" class="btn btn-block btn-danger btn-sm"
                                                        onclick="DeletePermission(this)">ลบ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <input type="hidden" value="" name="program_id">
    <input type="hidden" value="<?=$profile_detail['personDetail']['UserID']?>" name="user_id">
    <div style="text-align: center">
        <button type="button" class="btn btn-info btn-lg" onclick="AddCardManagePermission(this)" name="ButtonAddCardManagePermission"
                style="width: 40%;">เพิ่มสิทธิ์</button>
                &emsp;
        <button type="button" class="btn btn-success btn-lg" style="width: 40%;" onclick="SaveSystemDetail()">บันทึก</button>
    </div>
    <br><br>
</div>

<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/addprogram.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/accessright.js') ?>"></script>
