<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			ฟอร์มขอสิทธิ์การเข้าใช้งานระบบ
		</h1>
	</section>
	<section class="content">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">โปรดเลือกระบบ</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-remove"></i>
					</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>ชื่อระบบ</label>
							<select class="form-control select2" disabled name="ProgramName" onchange="SelectedProgram(this);" style="width: 100%;">
								<option selected value="<?=$program[0]['program_id']?>">
									<?=$program[0]['program_name']?>
								</option>
							</select>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">ฟอร์มข้อมูล</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-remove"></i>
					</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>สาเหตุการเปลี่ยนแปลงสิทธิ์</label>
							<select class="form-control" style="width: 100%;" name="MotiveChangePermission" disabled>
								<option <?php echo ($head[0][ 'cuase']=='1' ? 'selected' : null); ?> value="1" >ย้ายบริษัท</option>
								<option <?php echo ($head[0][ 'cuase']=='2' ? 'selected' : null); ?> value="2">ย้ายแผนก</option>
								<option <?php echo ($head[0][ 'cuase']=='3' ? 'selected' : null); ?> value="3">ลาออก</option>
								<option <?php echo ($head[0][ 'cuase']=='4' ? 'selected' : null); ?> value="4">อื่นๆ</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>ประเภทการเปลี่ยนแปลงสิทธิ์</label>
							<select class="form-control" style="width: 100%;" name="TypeChangePermission" disabled>
								<option <?php echo ($head[0][ 'type']=='1' ? 'selected' : null); ?> value="1">เพิ่มสิทธิ์</option>
								<option <?php echo ($head[0][ 'type']=='2' ? 'selected' : null); ?> value="2">ลดสิทธิ์</option>
								<option <?php echo ($head[0][ 'type']=='3' ? 'selected' : null); ?> value="3">ยกเลิกการใช้งานระบบ</option>
							</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<table class="table table-striped">
								<thead>
									<tr>
										<th style="width:20%;">ชื่อสิทธิ์ </th>
										<th style="width:80%;">รายละเอียด</th>
									</tr>
								</thead>
								<tbody id="role">
									<?php foreach ($role as $key => $value) {?>
									<tr>
										<td data-role-id="<?=$value['role_id']?>">
											<?php echo $value['role_name']; ?>
										</td>
										<td>
												<?php foreach ($value['func']['0'] as $key => $value_funt) {?>
												<?php foreach ($requestdetail as $key_detail => $value_redetail) {
															if ($value_redetail['role_id'] == $value['role_id'] && $value_redetail['function_id'] == $value_funt['function_id']) {
																echo '<div style="display: inline-block; min-width: 180px;">';
																if ($value_redetail['requestline'] == 1) {
																	echo "<input type='checkbox' class='minimal' requested='requested' name='funt_id' value='" . $value_funt['function_id'] . "'>  ";
																	echo "<color style='color:green'>" . $value_funt['function_name'] . "</color>&emsp;&emsp;";
																} else {
																	echo "<input type='checkbox' class='minimal' name='funt_id' value='" . $value_funt['function_id'] . "'>    ";
																	echo "<color style='color:red'>" . $value_funt['function_name'] . "</color>&emsp;&emsp;";
																}
																echo '</div>';
															} else {
															}
														}?>
												<?php }?>
										</td>
									</tr>
									<?php }?>
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" class='minimal' name="chk_all_request">&emsp;เลือกทั้งหมด
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">ผู้ร้องขอ</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>
						<th style="width: 25%">ชื่อผู้ใช้</th>
						<th style="width: 25%">วันที่</th>
						<th style="width: 25%">สถานะ</th>
						<th style="width: 25%">ตำแหน่ง</th>
					</tr>
					<tr>
						<td>
							<?php echo $profile_detail['personDetail']['PersonFnamethai'] . "    " . $profile_detail['personDetail']['PersonLnamethai']; ?>
						</td>
						<td>
							<?php 
							$date=date_create($head[0]['request_date']);
							echo date_format($date,'d/m/Y'); 
							?>
						</td>
						<td>
							<span class="label label-warning">Pending</span>
						</td>
						<td>ผู้ขอเปลี่ยนแปลงสิทธิ์</td>
					</tr>
					<tr>
						<td>
							<?php echo $profile_detail['personDetail']['PersonFnamethai'] . "    " . $profile_detail['personDetail']['PersonLnamethai'] ?>
						</td>
						<td>
							<?php 
							$date=date_create($head[0]['request_date']);
							echo date_format($date,'d/m/Y'); 
							?>
						</td>
						<td>
							<span class="label label-warning">Pending</span>
						</td>
						<td>ผู้ใช้งาน</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">ผู้อนุมัติ</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tr>
						<th style="width: 25%">ชื่อผู้ใช้</th>
						<th style="width: 25%">วันที่</th>
						<th style="width: 25%">สถานะ</th>
						<th style="width: 25%">ตำแหน่ง</th>
					</tr>
					<tr>
						<td>
							<p><?php echo $head[0]['User_NameT'].' '.$head[0]['SurNameT'] ?></p>
						</td>
						<td>-</td>
						<td>
							<span class="label label-success"></span>-</td>
						<td>DM/Asst.BM/GM/BM/MD</td>
					</tr>
					<tr>
						<td>-</td>
						<td>-</td>
						<td>
							<span class="label label-warning"></span>-</td>
						<td>Administrator</td>
					</tr>
				</table>
			</div>
		</div>
	</section>
	<div style="text-align: center">
		<button type="button" class="btn  btn-lg" style="width: 30%;">
			ย้อนกลับ</button>
		<button type="button" class="btn btn-danger btn-lg" style="width: 30%;" onclick="leader_notapprove()">ไม่อนุมัติ</button>
		<button type="button" class="btn btn-success btn-lg" style="width: 30%;" onclick="leader_approve()">อนุมัติ</button>
	</div>
	<div style="text-align: center">
		<button type="button" class="btn  btn-lg" style="width: 90%;">
			ย้อนกลับ</button>
	</div>
	<br>
	<br>
</div>

<!-- Hidden -->
<input type="hidden" name="RequestId" value="<?php echo $head[0]['ID']; ?>">


<!-- script -->
<script src="<?php echo base_url('assets/dist/js/user/approvepermissionform.js') ?>"></script>
