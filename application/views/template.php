<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AccessRight | Dashboard</title>
    <?php echo $__html['styles']; ?>
    <?php echo $__html['scripts']; ?>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32">

<?php echo $__html['header']; ?>

<?php echo $__html['menu']; ?>

<?php echo $__html['content']; ?>

<script src="<?php echo base_url('assets/dist/js/user/approvepermissionform.js') ?>"></script>
</body>
</html>
