<?php
 
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class Request_access_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        //Do your magic here


        //  $this->db=$this->load->database();
        //  $this->db2 = $this->load->database('local', TRUE);
        $this->db = $this->load->database('program', true);
       // $this->db4 = $this->load->database('test', true);
    }    
     public function newRequest($data_insert,$function_data)
     {
         $this->db->insert('request_access',$data_insert);
         $rq_id =  $this->db->insert_id();
         foreach ($function_data as  $value) {
             foreach ($value[1] as  $data) {
                 $data_insert2 = array("request_id"=>$rq_id,"role_id"=>$value[0],"function_id"=>$data,"allowed"=>0);
                 $this->db->insert('request_line',$data_insert2);
                }
            
         }
         
     }
     
     public function get_request_by_owner($uid)
     {
         $query=$this->db->query("EXEC find_new_request $uid");
         if($query->num_rows()>0){
            $data=$query->result_array();
            $data['count'] = $query->num_rows();
            return $data;
        }
        else { return 0;}
     }

     public function get_request_by_approver($approver_id)
     {
         $query = $this->db->query("EXEC get_request_by_approver $approver_id");
         if($query->num_rows()>0){
            $data=$query->result_array();
            $data['count'] = $query->num_rows();
            return $data;
        }
        else { return 0;}
     }

     public function get_all_request_by_user($uid)
     {
        $query = $this->db->query("select  * from request_access where user_id = $uid");
        if($query->num_rows()>0){
           $data=$query->result_array();
           $data['count'] = $query->num_rows();
           return $data;
       }
       else { return 0;}
    }

     public function get_request_line($pid,$rqid)
     {
        $query = $this->db->query("EXEC request_line_status @pid=$pid ,@rid=$rqid");
        if($query->num_rows()>0){
           $data=$query->result_array();
          
           return $data;
       }
       else { return 0;}
     }

     public function get_all_request_toadmin()
     {
        $query = $this->db->query("select  * from request_access where approve_status = 0 or it_approve != 3 ");
        if($query->num_rows()>0){
           $data=$query->result_array();
           $data['count'] = $query->num_rows();
           return $data;
       }
       else { return 0;}
     }
    public function approver_by_it($rqid)
     {
        $query = $this->db->query("update request_access set it_approve_status = 3 where ID = $rqid ");
        return $query;
     }
     public function approver_by_senior($rqid)
     {
        $query = $this->db->query("update request_access set it_approve_status = 2,approve_status = 1 where ID = $rqid ");
        return $query;
     }
     public function get_request_detail($rqid)
     {
        $query = $this->db->query("select  * from request_access rq
        inner join (select * from openquery(SERVICEDESKDB2, 'select User.* 
        from servicedeskdb2.User')) tb on tb.UserID = rq.approved_by where ID = $rqid ");
        if($query->num_rows()>0){
           $data=$query->result_array();
         //  $data['count'] = $query->num_rows();
           return $data;
       }
       else { return 0;}
     }
 
 }
 
 /* End of file ModelName.php */
 