<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class user_model extends CI_Model {
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
    
    
            $this->db = $this->load->database('program', true);
            //  $this->db2 = $this->load->database('local', TRUE);
            $this->db3 = $this->load->database('user', true);
           // $this->db4 = $this->load->database('test', true);
           
        }
        public function getpic($data)
        {
            foreach ($data['personDetail'] as $key => $value) {
                # code...
                $val[$key] = $value;
            }
            $sql = "select picfile from User where CitizenID = ";
            $sql .= "'" . $val['CitizenID'] . "'";
            $query = $this->db3->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = '';
            }
            $query->free_result();
            return $data[0]->picfile;
        }
    
        public function getprofiledetail($data)
        {
            foreach ($data['personDetail'] as $key => $value) {
                # code...
                $val[$key] = $value;
            }
            $sql ="select * from User ";
            $sql .=" inner join User_Group on User_Group.UserID = User.UserID";
            $sql .=" INNER JOIN Position on Position.PositionID = User_Group.PositionID ";
            $sql .=" WHERE User.CitizenID =".$val['CitizenID']; 

    $query = $this->db3->query($sql);
    if ($query->num_rows() > 0) {
        $data = $query->result_array();
        foreach ($data  as $val){

        }

    } else {
        $val = '';
            }
    $query->free_result();
    return $val;
    }
    
    public function get_all_IT_person()
    {
        $sql ="select u.userid,u.user_nameT,u.surnamet,u.ex_mail from User u   inner join User_Group ug on ug.UserID = u.UserID    where ug.GroupID in(1291,1290,1292,1288)";
        $query = $this->db3->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else return 0;
    }

    public function get_approver($cmp)
    {
        $query=$this->db->query("EXEC approver $cmp");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else return 0;
    }

    public function check_user_status($uid)
    {
        $query=$this->db->query("EXEC check_it_person $uid");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else return 0;
    }

}
    
    /* End of file user.php */
    