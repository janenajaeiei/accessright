<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here


        //  $this->db=$this->load->database();
        //  $this->db2 = $this->load->database('local', TRUE);
        $this->db = $this->load->database('program', true);
       // $this->db4 = $this->load->database('test', true);
    }    
    public function createprogram($data)
    {
    $this->db->insert('program',$data);
    $insert_id = $this->db->insert_id();

    return  $insert_id;
    }
    
    public function getallprogramshow()
    {
       $query=$this->db->query("select * from program where show = 1 ");
        if($query->num_rows()>0){
            $data=$query->result_array();
            return $data;
        }
        else { return 0;}
    }
    public function hideprogram($id)
    {
        $this->db->query("update program set show = 0 where program_id = '$id'");
    }

    public function editprogramdetail($id,$data)
    {   $data_insert = array('program_name'=>$data);
        $this->db->where('program_id',$id);
        $this->db->update('program',$data_insert);
        # code...
    }

    public function unhideprogram($id)
    {   $data = array('show' => 1  );
        $this->db->where('program_id',$id);
        $this->db->update('program',$data);
        # code...
    }
     public function add_new_role($pid,$data)
   {

      
          $data_insert = array("program_id"=>$pid,"role_name"=>$data);
          $this->db->insert('role',$data_insert);
          $query=$this->db->query("select max(role_id) as role_id from role where program_id = $pid");
          return $query->result_array();
          # code...
      
   }
   public function hide_role($id,$program_id)
   {
       $this->db->query("update role set show = 0 where program_id = '$program_id' and role_order = '$id'");
   }

   public function unhide_role($id,$program_id)
   {
       $this->db->query("update role set show = 1 where program_id = '$program_id' and role_order = '$id'");
   }

   
   public function get_role($program_id)
   {
      $query=$this->db->query("select * from role where program_id='$program_id' and show = 1");
      if($query->num_rows()>0){
        $data=$query->result_array();
        return $data;
    }
    else { return 0;}
   }
   public function add_program_owner($data_insert)
   {
      
           $this->db->insert('owner',$data_insert);
       
   }
   public function add_program_function($pid,$role_id,$function_data)
   {

           foreach ($function_data as  $value) {
               $data_insert = array('program_id' => $pid, 'role_id' => $role_id, 'function_name' => $value);
               $this->db->insert('function_role', $data_insert);

           }
   }
    public function get_program_detail($pid)
    {
        $query=$this->db->query("select * from program where program_id = $pid and show = 1");
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        else return false;
    }
    public function get_role_detail($pid)
    {
        $data = array();
        
        $query=$this->db->query("select * from role where program_id = $pid and show = 1");
        if ($query->num_rows()>0){
            foreach ($query->result_array() as $key => $value) {
                $value['func'] = [];
                $query_func=$this->db->query("select * from function_role where program_id = $pid and role_id = ".$value['role_id']." and show = 1");
                if($query_func->num_rows()>0){
                    array_push($value['func'],$query_func->result_array());
                }
                else  array_push($value['func'],"ไม่มีรายการ");
            array_push($data,$value);
            }
            return $data;
        }
        else return NULL;
    }
    public function get_function_detail($pid)
    {
        $query=$this->db->query("select * from function_role where program_id = $pid and show = 1");
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        else return NULL;
    }


    public function edit_function($pid,$role_id,$func_id,$func_name)
    {
       $this->db->query("update function_role set function_name = '".$func_name."' where program_id = $pid and role_id  = $role_id and function_id = $func_id");
    }
    public function edit_role_name($pid,$role_id,$role_name)
    {
       $this->db->query("update role set role_name = '".$role_name."' where program_id = $pid and role_id  = $role_id");
    }

    public function select_program_owner($pid)
    {
        $query = $this->db->query("EXEC get_program_owner @pid=$pid");
        if($query->num_rows()>0){
           $data=$query->result_array();
          
           return $data;
       }
       else { return 0;}
    }

    public function hide_function($program_id,$rid,$func_id)
    {
        $this->db->query("update role set show = 0 where program_id = '$program_id' and role_id = '$rid' and function_id = $func_id");
    }
}

/* End of file Program_model.php */
